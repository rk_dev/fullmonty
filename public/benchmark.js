var cubeRotation = 0.0;

main();

//
// Start here
//
function main() {
    const numTest = 20000;

    const canvas = document.querySelector('#glcanvas');
    const gl = canvas.getContext('webgl');

    const internalWidth = gl.drawingBufferWidth;
    const internalHeight = gl.drawingBufferHeight;
    const screenWidth = internalWidth;
    const screenHeight = internalHeight;

    const frameBuffer = gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER, frameBuffer);

    const depthRenderBuffer = gl.createRenderbuffer();
    gl.bindRenderbuffer(gl.RENDERBUFFER, depthRenderBuffer);
    gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, internalWidth, internalHeight);
    gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, depthRenderBuffer);

    const fTexture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, fTexture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, internalWidth, internalHeight, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);

    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, fTexture, 0);

    gl.bindTexture(gl.TEXTURE_2D, null);
    gl.bindRenderbuffer(gl.RENDERBUFFER, null);
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);

    gl.frontFace(gl.CW);
    gl.enable(gl.CULL_FACE);

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    let vs = [];
    for(let i=0; i<numTest; ++i){
        let v = vec2.create();
        const vx = 5 * (Math.random() * 2 - 1);
        const vy = 5 * (Math.random() * 2 - 1);
        vec2.set(v, vx, vy);
        vs.push(v);
    }

    const hw = internalWidth / 2;
    const hh = internalHeight / 2;

    function positionUpdated(x, y, v){
        x = x + v[0];
        y = y + v[1];
        return [x, y];
    }
    function rectUpdate(pos, texCoords, i, animIndex){
        const j = i * 12;
        const v = vs[i];
        [pos[j], pos[j+1]] = positionUpdated(pos[j], pos[j+1], v);
        [pos[j+3], pos[j+4]] = positionUpdated(pos[j+3], pos[j+4], v);
        [pos[j+6], pos[j+7]] = positionUpdated(pos[j+6], pos[j+7], v);
        [pos[j+9], pos[j+10]] = positionUpdated(pos[j+9], pos[j+10], v);

        const l = pos[j]; 
        const r = pos[j+3]; 
        const t = pos[j+1]; 
        const b = pos[j+7];
        if(l <= -hw || r >= hw){
            vs[i][0] *= -1;
        } 
        if(t <= -hh || b >= hh){
            vs[i][1] *= -1;
        } 

        const k = i * 8;
        const lc = animIndex * 1.0 / 6.0;
        const tc = 0;
        const rc = lc + 1.0 / 6.0;
        const bc = tc + 1.0 / 4.0;

        texCoords[k] = lc;
        texCoords[k+1] = tc;
        texCoords[k+2] = rc;
        texCoords[k+3] = tc;
        texCoords[k+4] = rc;
        texCoords[k+5] = bc;
        texCoords[k+6] = lc;
        texCoords[k+7] = bc;
    }

    // If we don't have a GL context, give up now

    if (!gl) {
        alert('Unable to initialize WebGL. Your browser or machine may not support it.');
        return;
    }

    // Vertex shader program

    const vsSource = `
        attribute vec4 aVertexPosition;
        attribute vec2 aTextureCoord;

        uniform mat4 uModelViewMatrix;
        uniform mat4 uProjectionMatrix;

        varying highp vec2 vTextureCoord;

        void main(void) {
            gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
            vTextureCoord = aTextureCoord;
        }
  `;

    // Fragment shader program

    const fsSource = `
        varying highp vec2 vTextureCoord;

        uniform sampler2D uSampler;

        void main(void) {
            gl_FragColor = texture2D(uSampler, vTextureCoord);
            if(gl_FragColor.a < 0.5){ discard; }
        }
  `;

    // Initialize a shader program; this is where all the lighting
    // for the vertices and so forth is established.
    const shaderProgram = initShaderProgram(gl, vsSource, fsSource);

    // Collect all the info needed to use the shader program.
    // Look up which attributes our shader program is using
    // for aVertexPosition, aTextureCoord and also
    // look up uniform locations.
    const programInfo = {
        program: shaderProgram,
        attribLocations: {
            vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
            textureCoord: gl.getAttribLocation(shaderProgram, 'aTextureCoord'),
        },
        uniformLocations: {
            projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
            modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
            uSampler: gl.getUniformLocation(shaderProgram, 'uSampler'),
        }
    };

    // Here's where we call the routine that builds all the
    // objects we'll be drawing.
    const screenBuffer = initScreenBuffer(gl, screenWidth, screenHeight);
    const [buffers, rawPositions, rawTextureCoords] = initBuffers(gl, 32, 40, numTest);
    const textureU1 = loadTexture(gl, 'u1.png');
    const textureWR = loadTexture(gl, 'wr.png');

    let timeAccumulate = [];
    let timeAccumulateLimit = 60;
    let last = 0;
    let animationCount = 0;
    let animeProgress = 0;
    let animationIndices = [0, 1, 2, 1];
    // Draw the scene repeatedly
    function render(now) {
        const elapsed = now - last;
        last = now;

        timeAccumulate.push(elapsed);
        if(timeAccumulate.length > timeAccumulateLimit){
            const avg = timeAccumulate.reduce((l, r) => l + r) / timeAccumulateLimit;
            const fps = 1000 / avg;
            document.getElementById("fps").innerText = "FPS: " + fps;
            timeAccumulate = [];
        }

        gl.bindFramebuffer(gl.FRAMEBUFFER, frameBuffer);
        prepareScene(gl, programInfo, buffers, textureWR, internalWidth, internalHeight);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        drawScene(gl, numTest);

        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        prepareScene(gl, programInfo, screenBuffer, fTexture, screenWidth, screenHeight);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        drawScene(gl, 1);

        const animationIndex = animationIndices[animeProgress % animationIndices.length];
        for(let i=0; i< numTest; ++i){
            rectUpdate(rawPositions, rawTextureCoords, i, animationIndex);
        }
        animationCount += 1;
        if(animationCount >= 10){
            animationCount = 0;
            animeProgress = (animeProgress + 1) % animationIndices.length;
        }

        gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, rawPositions);
        gl.bindBuffer(gl.ARRAY_BUFFER, buffers.textureCoord);
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, rawTextureCoords);

        requestAnimationFrame(render);
    }
    requestAnimationFrame(render);
}

function initScreenBuffer(gl, width, height) {

    const positionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

    const lp = -width / 2;
    const tp = -height / 2;
    const rp = width / 2;
    const bp = height / 2;

    const positions = [
        lp, tp, 1.0,
        rp, tp, 1.0,
        rp, bp, 1.0,
        lp, bp, 1.0,
    ];

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

    const textureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, textureCoordBuffer);

    const lc = 0.0;
    const tc = 1.0;
    const rc = 1.0;
    const bc = 0.0;
    const textureCoordinates = [
        lc, tc,
        rc, tc,
        rc, bc,
        lc, bc,
    ];

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoordinates),
        gl.STATIC_DRAW);

    const indexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);

    const indices = [
        0, 1, 2, 0, 2, 3,
    ]

    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
        new Uint16Array(indices), gl.STATIC_DRAW);

    return {
        position: positionBuffer,
        textureCoord: textureCoordBuffer,
        indices: indexBuffer,
    }
}

//
// initBuffers
//
// Initialize the buffers we'll need. For this demo, we just
// have one object -- a simple three-dimensional cube.
//
function initBuffers(gl, imageWidth, imageHeight, numTest) {

    // Create a buffer for the cube's vertex positions.

    const positionBuffer = gl.createBuffer();

    // Select the positionBuffer as the one to apply buffer
    // operations to from here out.

    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

    // Now create an array of positions for the cube.

    const lp = -imageWidth / 2;
    const tp = -imageHeight / 2;
    const rp = imageWidth / 2;
    const bp = imageHeight / 2;

    const positions = [];
    for(let i=0; i<numTest; ++i){
        const z = -(0.1 + 90.9 * i / numTest);
        positions.push(
            // Front face
            lp, tp, z,
            rp, tp, z,
            rp, bp, z,
            lp, bp, z,
        );
    }

    // Now pass the list of positions into WebGL to build the
    // shape. We do this by creating a Float32Array from the
    // JavaScript array, then use it to fill the current buffer.

    let rawPositions = new Float32Array(positions)

    gl.bufferData(gl.ARRAY_BUFFER, rawPositions, gl.DYNAMIC_DRAW);

    // Now set up the texture coordinates for the faces.

    const textureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, textureCoordBuffer);

    const lc = 0.0;
    const tc = 0.0;
    const rc = 1.0 / 6.0;
    const bc = 1.0 / 4.0;
    const textureCoordinates = []

    for(let i=0; i<numTest; ++i){
        textureCoordinates.push(
            // Front
            lc, tc,
            rc, tc,
            rc, bc,
            lc, bc,
        );
    }

    let rawTextureCoordinates = new Float32Array(textureCoordinates)
    gl.bufferData(gl.ARRAY_BUFFER, rawTextureCoordinates, gl.DYNAMIC_DRAW);

    // Build the element array buffer; this specifies the indices
    // into the vertex arrays for each face's vertices.

    const indexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);

    // This array defines each face as two triangles, using the
    // indices into the vertex array to specify each triangle's
    // position.

    const indices = [];
    
    for(let i=0; i<numTest; ++i){
        const j = i * 4;
        indices.push(
            // Front
            j, j+1, j+2, j, j+2, j+3,    // front
        );
    }

    // Now send the element array to GL

    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
        new Uint16Array(indices), gl.STATIC_DRAW);

    return [
        {
            position: positionBuffer,
            textureCoord: textureCoordBuffer,
            indices: indexBuffer,
        },
        rawPositions,
        rawTextureCoordinates
    ]
}

//
// Initialize a texture and load an image.
// When the image finished loading copy it into the texture.
//
function loadTexture(gl, url) {
    const texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);

    // Because images have to be download over the internet
    // they might take a moment until they are ready.
    // Until then put a single pixel in the texture so we can
    // use it immediately. When the image has finished downloading
    // we'll update the texture with the contents of the image.
    const level = 0;
    const internalFormat = gl.RGBA;
    const width = 1;
    const height = 1;
    const border = 0;
    const srcFormat = gl.RGBA;
    const srcType = gl.UNSIGNED_BYTE;
    const pixel = new Uint8Array([0, 0, 255, 255]);  // opaque blue
    gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
        width, height, border, srcFormat, srcType,
        pixel);

    const image = new Image();
    image.onload = function () {
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
            srcFormat, srcType, image);

        // WebGL1 has different requirements for power of 2 images
        // vs non power of 2 images so check if the image is a
        // power of 2 in both dimensions.
        if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
            // Yes, it's a power of 2. Generate mips.
            gl.generateMipmap(gl.TEXTURE_2D);
        } else {
            // No, it's not a power of 2. Turn of mips and set
            // wrapping to clamp to edge
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        }
    };
    image.src = url;

    return texture;
}

function isPowerOf2(value) {
    return (value & (value - 1)) == 0;
}

//
// Draw the scene.
//
function prepareScene(gl, programInfo, buffers, texture, width, height){
    gl.clearColor(0.5, 0.8, 0.8, 1.0);  // Clear to black, fully opaque
    gl.clearDepth(1.0);                 // Clear everything
    gl.enable(gl.DEPTH_TEST);           // Enable depth testing
    gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

    gl.enable(gl.BLEND);
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

    const zNear = 0.1;
    const zFar = 100.0;
    const projectionMatrix = mat4.create();

    // note: glmatrix.js always has the first argument
    // as the destination to receive the result.
    const l = -width / 2;
    const r = width / 2;
    const t = -height / 2;
    const b = height / 2;
    mat4.ortho(projectionMatrix,
        l,
        r,
        b,
        t,
        zNear, 
        zFar
    );

    // Tell WebGL how to pull out the positions from the position
    // buffer into the vertexPosition attribute
    {
        const numComponents = 3;
        const type = gl.FLOAT;
        const normalize = false;
        const stride = 0;
        const offset = 0;
        gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
        gl.vertexAttribPointer(
            programInfo.attribLocations.vertexPosition,
            numComponents,
            type,
            normalize,
            stride,
            offset);
        gl.enableVertexAttribArray(
            programInfo.attribLocations.vertexPosition);
    }

    // Tell WebGL how to pull out the texture coordinates from
    // the texture coordinate buffer into the textureCoord attribute.
    {
        const numComponents = 2;
        const type = gl.FLOAT;
        const normalize = false;
        const stride = 0;
        const offset = 0;
        gl.bindBuffer(gl.ARRAY_BUFFER, buffers.textureCoord);
        gl.vertexAttribPointer(
            programInfo.attribLocations.textureCoord,
            numComponents,
            type,
            normalize,
            stride,
            offset);
        gl.enableVertexAttribArray(
            programInfo.attribLocations.textureCoord);
    }

    // Tell WebGL which indices to use to index the vertices
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);

    // Tell WebGL to use our program when drawing

    gl.useProgram(programInfo.program);

    const modelViewMatrix = mat4.create();

    mat4.translate(modelViewMatrix,     // destination matrix
        modelViewMatrix,     // matrix to translate
        [0, 0, -6.0]);  // amount to translate

    gl.uniformMatrix4fv(
        programInfo.uniformLocations.modelViewMatrix,
        false,
        modelViewMatrix);

    gl.uniformMatrix4fv(
        programInfo.uniformLocations.projectionMatrix,
        false,
        projectionMatrix);

    // Specify the texture to map onto the faces.

    // Tell WebGL we want to affect texture unit 0
    gl.activeTexture(gl.TEXTURE0);

    // Bind the texture to texture unit 0
    gl.bindTexture(gl.TEXTURE_2D, texture);

    // Tell the shader we bound the texture to texture unit 0
    gl.uniform1i(programInfo.uniformLocations.uSampler, 0);
}

function drawScene(gl, numTest) {
    {
        const vertexCount = 6 * numTest;
        const type = gl.UNSIGNED_SHORT;
        const offset = 0;
        gl.drawElements(gl.TRIANGLES, vertexCount, type, offset);
    }
}

//
// Initialize a shader program, so WebGL knows how to draw our data
//
function initShaderProgram(gl, vsSource, fsSource) {
    const vertexShader = loadShader(gl, gl.VERTEX_SHADER, vsSource);
    const fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fsSource);

    // Create the shader program

    const shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    // If creating the shader program failed, alert

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
        return null;
    }

    return shaderProgram;
}

//
// creates a shader of the given type, uploads the source and
// compiles it.
//
function loadShader(gl, type, source) {
    const shader = gl.createShader(type);

    // Send the source to the shader object

    gl.shaderSource(shader, source);

    // Compile the shader program

    gl.compileShader(shader);

    // See if it compiled successfully

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
        gl.deleteShader(shader);
        return null;
    }

    return shader;
}
